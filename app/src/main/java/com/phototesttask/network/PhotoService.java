package com.phototesttask.network;

import com.phototesttask.network.domain.ServiceDocument;
import com.phototesttask.network.domain.UserPhotos;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface PhotoService {

    @Headers({"Accept:application/json"})
    @GET("users/{user}/")
    Observable<ServiceDocument> getServiceDocument(@Path("user") String user);

    @Headers({"Accept:application/json"})
    @GET
    Observable<UserPhotos> getPhotos(@Url String url);
}