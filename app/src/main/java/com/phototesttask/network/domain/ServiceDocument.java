package com.phototesttask.network.domain;

/**
 * Created by peter on 14/08/2017.
 */
import java.util.Map;

import static com.phototesttask.network.domain.Constants.PHOTOS_ID;

public class ServiceDocument {

    private Map<String, CollectionInfo> collections;

    public Map<String, CollectionInfo> getCollections() {
        return collections;
    }

    public void setCollections(Map<String, CollectionInfo> collections) {
        this.collections = collections;
    }

    public String getPhotosUrl() {
        return collections.isEmpty() ? null : collections.get(PHOTOS_ID).getHref();
    }
}