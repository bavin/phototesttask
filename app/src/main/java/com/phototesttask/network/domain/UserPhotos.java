package com.phototesttask.network.domain;

import java.util.List;

/**
 * Created by peter on 14/08/2017.
 */

public class UserPhotos {
    private List<PhotoEntry> entries;

    public List<PhotoEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<PhotoEntry> entries) {
        this.entries = entries;
    }
}


