package com.phototesttask.network.domain;

/**
 * Created by peter on 15/08/2017.
 */
public class PhotoInfo {
    private Integer width;
    private Integer height;
    private String href;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
