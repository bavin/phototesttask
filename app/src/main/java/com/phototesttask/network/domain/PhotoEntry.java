package com.phototesttask.network.domain;

import com.phototesttask.utils.QualityHelper;
import com.phototesttask.utils.PhotoQuality;

import java.util.Map;

/**
 * Created by peter on 15/08/2017.
 */
public class PhotoEntry {
    private Map<String, PhotoInfo> img;
    private String title;

    public Map<String, PhotoInfo> getImg() {
        return img;
    }

    public void setImg(Map<String, PhotoInfo> img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl(PhotoQuality quality) {
        if (getImg().isEmpty()) {
            return null;
        }

        PhotoQuality desiredQuality = quality;
        PhotoInfo desiredPhoto = null;

        while (true) {
            desiredPhoto = getImg().get(desiredQuality.getQualityLabel());
            if (desiredPhoto != null) {
                return desiredPhoto.getHref();
            }
            desiredQuality = QualityHelper.getClosestLowerQuality(desiredQuality);
        }
    }
}