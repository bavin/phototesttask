package com.phototesttask.ui;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class CarouselLayoutManager extends LinearLayoutManager {

    private final static float MINIMUM_SIZE = 0.8f;

    public CarouselLayoutManager(Context context) {
        super(context);
    }

    public CarouselLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        int orientation = getOrientation();
        if (orientation == VERTICAL) {
            int scrolled = super.scrollVerticallyBy(dy, recycler, state);
            float middle = getHeight() / 2f;

            float x0 = middle;
            float x1 = 0f;

            float y0 = 1f;
            float y1 = MINIMUM_SIZE;

            for (int i = 0; i < getChildCount(); i++) {
                View child = getChildAt(i);

                float childMidpoint = (getDecoratedBottom(child) + getDecoratedTop(child)) / 2f;
                float x = Math.abs(middle - childMidpoint);

                // linear interpolation for smooth animation
                float scale = y0 + (y1 - y0) * (x - x1) / (x0 - x1);

                child.setScaleX(scale);
                child.setScaleY(scale);
            }

            return scrolled;
        } else {
            return 0;
        }
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        super.onLayoutChildren(recycler, state);
        scrollVerticallyBy(0, recycler, state);
    }
}