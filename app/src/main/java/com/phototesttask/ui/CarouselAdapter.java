package com.phototesttask.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.phototesttask.R;

import java.util.List;

/**
 * Created by peter on 14/08/2017.
 */
public class CarouselAdapter extends RecyclerView.Adapter<CarouselAdapter.CardHolder> {

    private List<String> data;
    private Context context;

    public CarouselAdapter(List<String> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item, parent, false);
        return new CardHolder(view);
    }

    @Override
    public void onBindViewHolder(CardHolder holder, int position) {
        String url = data.get(position);

        Glide.with(context)
                .load(url)
                .error(R.drawable.ic_add_photo_fail)
                .thumbnail(0.1f)
                .crossFade()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class CardHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        CardHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
        }
    }

    public void setData(List<String> data) {
        this.data = data;
        notifyDataSetChanged();
    }
}
