package com.phototesttask.utils;

/**
 * Created by peter on 16/08/2017.
 */
public enum PhotoQuality {

    LDPI("S"),
    MDPI("M"),
    HDPI("L"),
    XLDPI("XL"),
    XXLDPI("XXL"),
    XXXLDPI("XXXL"),
    ORIG("orig");

    private final String qualityLabel;

    PhotoQuality(String qualityLabel) {
        this.qualityLabel = qualityLabel;
    }

    public String getQualityLabel() {
        return qualityLabel;
    }
}
