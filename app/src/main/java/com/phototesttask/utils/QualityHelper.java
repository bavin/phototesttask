package com.phototesttask.utils;

import android.content.Context;

import static com.phototesttask.utils.PhotoQuality.HDPI;
import static com.phototesttask.utils.PhotoQuality.LDPI;
import static com.phototesttask.utils.PhotoQuality.MDPI;
import static com.phototesttask.utils.PhotoQuality.ORIG;
import static com.phototesttask.utils.PhotoQuality.XLDPI;
import static com.phototesttask.utils.PhotoQuality.XXLDPI;
import static com.phototesttask.utils.PhotoQuality.XXXLDPI;

/**
 * Created by peter on 16/08/2017.
 */
public class QualityHelper {

    public static PhotoQuality getDesiredQuality(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density < 1.0f) {
            return LDPI;
        } else if (density >= 1.0f && density < 1.5f) {
            return MDPI;
        } else if (density == 1.5f) {
            return HDPI;
        } else if (density > 1.5f && density <= 2.0f) {
            return XLDPI;
        } else if (density > 2.0f && density <= 3.0f) {
            return XXLDPI;
        } else {
            return XXXLDPI;
        }
    }

    public static PhotoQuality getClosestLowerQuality(PhotoQuality quality) {
        switch (quality) {
            case XXXLDPI: return XXLDPI;
            case XXLDPI: return XLDPI;
            case XLDPI: return HDPI;
            case HDPI: return MDPI;
            case MDPI: return LDPI;
            case LDPI: return ORIG;
        }
        return ORIG;
    }
}