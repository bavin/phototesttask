package com.phototesttask.mvp;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

/**
 * Created by peter on 15/08/2017.
 */
public interface MainView extends MvpView{
    @StateStrategyType(SingleStateStrategy.class)
    void initAdapter(List<String> data);

    @StateStrategyType(SingleStateStrategy.class)
    void displayError(Throwable error);
}
