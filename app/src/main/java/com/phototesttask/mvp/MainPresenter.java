package com.phototesttask.mvp;

import android.content.Context;
import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.phototesttask.network.PhotoService;
import com.phototesttask.network.RestClient;
import com.phototesttask.network.domain.UserPhotos;
import com.phototesttask.utils.QualityHelper;
import com.phototesttask.utils.PhotoQuality;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.phototesttask.network.domain.Constants.TEST_PROFILE_NAME;

/**
 * Created by peter on 15/08/2017.
 */
@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    // Normally these injections are done with Dagger2.
    private Context context;
    private PhotoService service = RestClient.INSTANCE.getService(PhotoService.class);

    public MainPresenter(Context context) {
        this.context = context;
    }

    public void loadData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            return;
        }

        final PhotoQuality desiredQuality = QualityHelper.getDesiredQuality(context);

        service.getServiceDocument(TEST_PROFILE_NAME)
                .flatMap(serviceDocument -> service.getPhotos(serviceDocument.getPhotosUrl()))
                .subscribeOn(Schedulers.io())
                .flatMapIterable(UserPhotos::getEntries)
                .map(photoEntry -> photoEntry.getUrl(desiredQuality))
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError);
    }

    private void onSuccess(List<String> data) {
        getViewState().initAdapter(data);
    }
    private void onError(Throwable error) {
        getViewState().displayError(error);
    }
}
