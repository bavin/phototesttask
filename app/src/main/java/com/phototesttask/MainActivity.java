package com.phototesttask;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.phototesttask.mvp.MainPresenter;
import com.phototesttask.mvp.MainView;
import com.phototesttask.ui.CarouselAdapter;
import com.phototesttask.ui.CarouselLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private TextView hintTextView;
    private CarouselAdapter adapter;

    @InjectPresenter
    MainPresenter presenter;

    @ProvidePresenter
    public MainPresenter provideMainPresenter() {
        return new MainPresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        initViews();
        loadData(savedInstanceState);
    }

    private void bindViews() {
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        hintTextView = (TextView) findViewById(R.id.hintText);
    }

    private void initViews() {
        recyclerView.setLayoutManager(new CarouselLayoutManager(this));
        adapter = new CarouselAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(adapter);
        refreshLayout.setOnRefreshListener(() -> presenter.loadData(null));
    }

    private void loadData(Bundle savedInstanceState) {
        progressBar.setVisibility(View.VISIBLE);
        presenter.loadData(savedInstanceState);
    }

    @Override
    public void initAdapter(List<String> data) {
        refreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        hintTextView.setVisibility(data.isEmpty() ? View.VISIBLE : View.GONE);
        hintTextView.setText(R.string.no_photos_found);
        adapter.setData(data);
    }

    @Override
    public void displayError(Throwable error) {
        refreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        hintTextView.setVisibility(View.VISIBLE);
        hintTextView.setText(R.string.error_text);
        adapter.setData(new ArrayList<>());
    }
}
